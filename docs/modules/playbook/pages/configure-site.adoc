= Site Keys

The playbook keys configured under `site` manage the site's published identity and how it interacts with certain applications once it's published.

[#site-key]
== site key

Global generated site files, service accounts, and other common properties are defined under the `site` key in a playbook file.
These settings are applied to the entire site when it's generated.

[source,yaml]
----
site: # <1>
  title: Docs Site # <2>
  url: https://docs.example.org # <3>
  start_page: component-b::index.adoc # <4>
  robots: allow # <5>
  keys: # <6>
    google_analytics: XX-123456 # <7>
----
<1> Required `site` key
<2> Required `title` key
<3> Required `url` key
<4> Optional `start_page` key
<5> Optional `robots` key
<6> Optional `keys` key
<7> Optional `google_analytics` key

The `site`, `title`, and `url` keys are required.
The other keys are optional; Antora will use their default values if they're not specified.

[#site-reference]
== Available site keys

[cols="3,6,1"]
|===
|Site Keys |Description |Required

|xref:site-keys.adoc#google-analytics-key[keys.google_analytics]
|Associates a Google Analytics account with the site.
|No

|xref:site-robots.adoc[robots]
|Specifies whether Antora generates a _robots.txt_ file.
Accepts the values `allow`, `disallow`, and a custom, multi-line string.
Ignored if the sibling `url` key is not set.
|No

|xref:site-start-page.adoc[start_page]
|Accepts a page ID that specifies the start page of a site.
|No

|xref:site-title.adoc[title]
|Specifies the title of a site.
|Yes

|xref:site-url.adoc[url]
|Specifies the base URL of a site.
|Yes
|===
